# scrcpy-compose

A docker-compose setup to run [pierlon/scrcpy-docker](https://github.com/pierlon/scrcpy-docker).

## Why

I prefer to use a mount-bind, so my ADB keys persist wild `docker system prune` runs.

## How

Before running the image, Docker must be allowed to connect to the X server:

```shell
xhost + local:docker
```

Configure the  graphics if needed (defaults to `intel`):

```shell
cp .env.example .env
vi .env
```

Run it:

```shell
docker-compose run --rm scrcpy
```

## Upstream

* [pierlon/scrcpy-docker](https://github.com/pierlon/scrcpy-docker)
* [Genymobile/scrcpy](https://github.com/Genymobile/scrcpy)
